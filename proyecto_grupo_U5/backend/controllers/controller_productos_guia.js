const modeloProducto = require('../models/model_productos');

function productosListar(req, res)
{
    var idProductos = req.params.id;
    if (!idProductos)
    {
        var result = modeloProducto.find({});
    }
    else
    {
        var result = modeloProducto.find({ _id: idProductos});
    }
    result.exec(function (err, result)
    {
    if(err) 
    {
        res
        .status(500)
        .send({ message: "Error al momento de ejecutar la solicitud" });
    }
    else
    {
        if (!result)
        {
            res
            .status(404)
            .send({ message: "El registro a buscar no se encuentra disponible" });
        }
        else
        {
            res.status(200).send({ result });
        }
    }
    });
}

function productosAgregar(req, res)
{
    var miproducto = new modeloProducto(req.body);
    miproducto.save((err, result) => {
        res.status(200).send({ message: result });
    });
}

function productosEditar(req, res)
{
    var id = req.params.id;
    console.log(id)
    modeloProducto.findOneAndUpdate( { _id: id },req.body, { new: true },
    function (err, modeloProducto) {
        if (err) res.send(err);
        console.log(modeloProducto);
        res.json(modeloProducto);
        }
    );
}

function productosBorrar(req, res)
{
    var idProductos = req.params.id;
    modeloProducto.findByIdAndRemove(idProductos, function (err, Productos)
    {
    if(err)
    {
        return res.json(500, {
        message: "No hemos encontrado el usuario",
        });
    }
    return res.json(Productos);
    });
}

module.exports = {productosListar, productosAgregar, productosEditar, productosBorrar};
